import java.awt.*;

public class Rectangle {
    private Point upperLeftCorner;
    private double width;
    private double height;

    public Rectangle(Point upperLeftCorner, double width, double height) {
        this.upperLeftCorner = upperLeftCorner;
        this.width = width;
        this.height = height;
    }

    // Verschiebt das Rechteckt um die übergebenen Delta Werte in x- und y-Richtung
    public void move(Point delta) {
        this.upperLeftCorner.translate(delta.x, delta.y);
    }

    // Berechnet die Fläche des Rechtecks als Breite mal Höhe
    public double area() {
        return width * height;
    }

}
